# -*- coding: utf-8 -*-
# @Time    : 1/14/2022 1:49 PM
# @FileName: movie.py
# @Software: PyCharm
# @Author  : Zerlaer
# @Blog    ：https://zerlaer.com
# -*- coding: utf-8 -*-
# @Time    : 1/13/2022 1:04 PM
# @FileName: douban.py
# @Software: PyCharm
# @Author  : Zerlaer
# @Blog    ：https://zerlaer.com
import pandas as pd
import requests
from bs4 import BeautifulSoup as bs

def del_sign(s):
    '删除各种符号和空格'
    s = s.replace(' ', '').replace('\n', '').replace('\t', '').replace('\r', '')
    return s

class DoubanMovie(object):
    '''
    豆瓣电影Top250
    '''

    def __init__(self):
        self.url = 'https://movie.douban.com/top250?start='
        self.header = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36',
            'Cookie': 'bid=fTZqoDQvlio; __utmz=30149280.1642050262.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); gr_user_id=f3dfd219-2bd1-4136-8884-9ea82032d5ee; dbcl2="232325421:sIWR6S1EbNI"; push_noty_num=0; push_doumail_num=0; ck=rOA0; ap_v=0,6.0; __utma=30149280.777989402.1642050262.1642062958.1642137848.4; __utmc=30149280; __utmb=30149280.1.10.1642137848; _pk_ref.100001.4cf6=%5B%22%22%2C%22%22%2C1642139031%2C%22https%3A%2F%2Fwww.google.com%2F%22%5D; _pk_ses.100001.4cf6=*; __utma=223695111.793474241.1642139031.1642139031.1642139031.1; __utmb=223695111.0.10.1642139031; __utmc=223695111; __utmz=223695111.1642139031.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _pk_id.100001.4cf6=50ba70245f0de5eb.1642139031.1.1642141013.1642139031.'
        }

    def parse_html(self):
        titleList, linkList, infoList, ratingList = [], [], [], []
        for step in list(range(0, 226, 25)):
            response = requests.get(self.url + str(step), headers=self.header)
            print(response.status_code)
            if response.status_code == 200:
                response.encoding = response.apparent_encoding
                content = response.text
                soup = bs(content, 'html.parser')
                # content > div > div.article > ol > li:nth-child(1) > div > div.info > div.bd > div > span.rating_num
                # content > div > div.article > ol > li:nth-child(1) > div > div.info > div.hd > a
                title = soup.select('div.info > div.hd > a > span:nth-child(1)')
                info = soup.select('div.info > div.bd > p:nth-child(1)')
                link = soup.select('div.info > div.hd > a')
                rating = soup.select('div.info > div.bd > div > span.rating_num')
                for t, i, l, r in zip(title, info, link, rating):
                    titleList.append(del_sign(t.text))
                    infoList.append(del_sign(i.text))
                    linkList.append(del_sign(l.get('href')))
                    ratingList.append(del_sign(r.text))
                data = pd.DataFrame(zip(titleList, infoList, linkList, ratingList),
                                    columns=['电影名称', '电影信息', '豆瓣链接', '豆瓣评分'])  # 转为dataframe
                data.to_csv('豆瓣电影Top250.csv', encoding='utf-8-sig')  # 存为csv
            else:
                print('数据请求失败')


if __name__ == '__main__':
    movie = DoubanMovie()
    movie.parse_html()
