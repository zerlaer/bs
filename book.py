# -*- coding: utf-8 -*-
# @Time    : 1/13/2022 1:04 PM
# @FileName: douban.py
# @Software: PyCharm
# @Author  : Zerlaer
# @Blog    ：https://zerlaer.com
import pandas as pd
import requests
from bs4 import BeautifulSoup as bs


class DoubanBook(object):
    """
    豆瓣读书Top250
    """
    def __init__(self):
        self.url = 'https://book.douban.com/top250?start='
        self.header = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'}

    def parse_html(self):
        titleList, linkList, infoList, ratingList = [], [], [], []
        for step in list(range(0, 226, 25)):
            response = requests.get(self.url + str(step), headers=self.header)
            if response.status_code == 200:
                response.encoding = response.apparent_encoding
                content = response.text
                soup = bs(content, 'html.parser')
                title = soup.select("title.pl2 > a ")
                info = soup.select("p.pl")
                rating = soup.select("span.rating_nums")
                for a, p, s in zip(title, info, rating):
                    titleList.append(a.get('title'))
                    linkList.append(a.get('href'))
                    infoList.append(p.text)
                    ratingList.append(s.text)
                print("数据请求完成")
                data = pd.DataFrame(zip(titleList, infoList, linkList,ratingList),
                                    columns=['书籍名称', '书籍信息', '书籍链接', '书籍评分'])  # 转为dataframe
                data.to_csv('豆瓣读书Top250.csv', encoding='utf-8-sig')  # 存为csv
                print("数据存储完成")
            else:print("数据请求失败")
if __name__ == '__main__':
    book = DoubanBook()
    book.parse_html()
