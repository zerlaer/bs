import pandas as pd
import requests
from bs4 import BeautifulSoup

url = "https://s.weibo.com/top/summary?Refer=top_hot&topnav=1&wvr=6"

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36",
    'cookie': 'SUB=_2AkMWh1cGf8NxqwFRmPgdzWrgboh2zADEieKg26bdJRMxHRl-yT9kqlM8tRB6PQd56RDd_1M5To59gET-POkXJ4EAWxy_; SUBP=0033WrSXqPxfM72-Ws9jqgMF55529P9D9W5u_KLwKw8lD2.28Z-7IgFa; _s_tentry=passport.weibo.com; Apache=6418915584096.423.1641797689730; SINAGLOBAL=6418915584096.423.1641797689730; ULV=1641797689735:1:1:1:6418915584096.423.1641797689730:'
}
response = requests.get(url, headers=headers)
if response.status_code == 200:
    content = response.content.decode("utf-8")
    soup = BeautifulSoup(content, "lxml")
    information = soup.find_all('tr')
    information = information[2:]
    rank, news, zan_num = [], [], []

    for each in information:
        rank_temp = each.find('td', {'class': 'td-01 ranktop'})  # 获取排名
        if each.find('a', {'target': '_blank'}) is None:  # 获取新闻
            news.append(each.find('a')['word'])
        else:
            news.append(each.find('a', {'target': '_blank'}).get_text())
        num = each.find('span')  # 获取新闻
        rank.append(rank_temp.get_text())
        zan_num.append(num.get_text())
    words = list(zip(news, zan_num))
    # 存储数据
    data = pd.DataFrame(zip(news, zan_num), columns=['新闻标题', '新闻链接'])  # 转为dataframe
    data.to_csv('新浪微博热搜.csv', encoding='utf-8-sig')  # 存为csv
    print('数据请求完成！')
else:
    print('数据请求失败！')
