# -*- coding: utf-8 -*-
# @Time    : 1/13/2022 10:30 AM
# @FileName: title.py
# @Software: PyCharm
# @Author  : Zerlaer
# @Blog    ：https://zerlaer.com
import pandas as pd
import requests
from bs4 import BeautifulSoup


def del_sign(s):
    '删除各种符号和空格'
    s = s.replace(' ', '').replace('\n', '').replace('\t', '').replace('\r', '')
    return s


if __name__ == '__main__':
    # 参数设置
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}  # 请求头，模拟浏览器进行请求
    url = 'http://www.infzm.com'  # 要爬虫的网址

    # 开始爬取
    response = requests.get(url, headers=headers)  # 发送请求并得到响应
    titles, links = [], []  # 存储文章标题和概要容器
    if response.status_code == 200:
        response.encoding = response.apparent_encoding  # 字符编码设置为网页本来所属编码
        html = response.text  # 获取网页代码
        soup = BeautifulSoup(html, 'lxml')  # 将网页文本代码转为bs对象
        title = soup.find('div', attrs={'class', 'row'}).find_all('h5')  # 获取文章标签标题
        link = soup.find('div', attrs={'class', 'row'}).find_all('a')
        for i, j in zip(title, link):
            titles.append(del_sign(i.text))
            links.append(url+del_sign(j.get('href')))
        # 存储数据
        data = pd.DataFrame(zip(titles, links), columns=['新闻标题', '新闻链接'])  # 转为dataframe
        data.to_csv('南方周末新闻.csv', encoding='utf-8-sig')  # 存为csv
        print('请求完成！')
    else:
        print('请求失败！')
